#!/bin/bash
set -ex
spock-shell --with default-c,default-c++,boost-nopy-1.78 --install bash -c \
	    'c++ -Wall -g -pthread -I$BOOST_ROOT/include -o blame-warnings blame-warnings.C -Wl,-rpath,$BOOST_ROOT/lib -L$BOOST_ROOT/lib -lboost_filesystem -lboost_regex -lboost_system -lboost_atomic'

HERE=$(pwd)
(
    cd $HOME/bin
    rm -f blame-warnings
    ln -s $HERE/blame-warnings .
)
