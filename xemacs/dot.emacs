;; Robb's emacs startup file                                                                                            -*- lisp -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MELPA
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; Check if .emacs.init exists in the home directory, and if so, load it and rename it to .emacs.init.orig.
(let ((init-file (expand-file-name ".emacs.init" (getenv "HOME"))))
  (when (file-exists-p init-file)
    (load-file init-file)
    (rename-file init-file (concat init-file ".orig"))))

;; Note that you'll need to run M-x package-refresh-contents or M-x package-list-packages to ensure that Emacs has fetched the MELPA
;; package list before you can install packages with M-x package-install or similar.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Basic global settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'load-path (expand-file-name "~/.emacs.d/lisp"))

(setq inhibit-startup-message t)             ; do not display startup message
(setq enable-recursive-minibuffers t)        ; allow minibuffer use in minibuffer
(put 'narrow-to-region 'disabled nil)        ; allow C-x n n
(put 'eval-expression 'disabled nil)         ; allow M-:
(put 'upcase-region 'disabled nil)           ; allow C-x C-u
(put 'downcase-region 'disabled nil)         ; allow C-x C-l
(setq enable-local-variables 'query)         ; ask about LISP in files
(setq enable-local-eval 'query)              ; ask about `eval' in files
(setq version-control t)                     ; use backup version numbers
(setq kept-old-versions 2)                   ; keep first two original versions
(setq kept-new-versions 10)                  ; keep 10 most recent versions
(setq visible-bell t)                        ; be quiet, but flash since sound is muted by default on Robb's machines
(setq next-line-add-newlines nil)            ; do not add lines at end of buffer
(setq line-number-mode t)                    ; turn on line numbers in mode line
(setq column-number-mode t)                  ; turn on column numbers in mode line
(setq read-quoted-char-radix 16)             ; enter quoted chars in hexadecimal instead of octal
(set-cursor-color "green")                   ; to be visible even with reverse video
(setq gc-cons-threshold 100000000)           ; allow larger expressions on modern machines
(setq read-process-output-max (* 1024 1024)) ; allow more input on larger machines
(display-time)                               ; show current time and load level in the mode line
(global-display-line-numbers-mode 1)         ; show line numbers in all buffers
(setq sml/no-confirm-load-theme t)           ; yes, we know themes can run LISP code                                    

(setq blink-matching-parent t)
(setq column-number-mode t)
(setq compilation-context-lines 2)
(setq compilation-scroll-output t)
(setq compilation-skip-threshold 0)
(setq compilation-window-height 25)
(setq delete-old-versions t)
(setq helm-completion-style 'helm)
(setq hide-ifdef-initially t)
(setq hide-ifdef-shadow t)
(setq magit-log-section-commit-count 40)
(setq next-error-highlight t)
(setq org-agenda-files nil)
(setq scroll-bar-mode 'right)
(setq show-paren-style 'mixed)
(setq transient-mark-mode nil)
(setq which-function-mode t)

;; The split-window-sensibly is insensible. Splitting windows so they're side-by-side rather than one on top of the other doesn't
;; work well when the user has adjusted the frame size to be suitable for editing. If the code is up to 132 columns wide and the
;; user has disabled truncate-lines then splitting in half side by side only makes sense if the window is at least 2 * 132 + n
;; characters wide (where "n" accounts for however much additional space would be lost due to scroll bars, gutters, etc.
(setq split-width-threshold nil)
(setq split-height-threshold nil)

;; Additional key bindings
(global-set-key (kbd "<f3>") 'goto-line)
(global-set-key (kbd "<C-f3>") 'goto-char)
(global-set-key (kbd "<f4>") 'magit-status)
(global-set-key (kbd "<C-f4>") 'magit-dispatch-popup)
(global-set-key (kbd "<f5>") 'compile)
(global-set-key (kbd "<C-f5>") 'grep)

;; Key bindings to adjust font size in a single buffer.
(global-set-key (kbd "C-+") (lambda () (interactive) (text-scale-increase 1)))
(global-set-key (kbd "C--") (lambda () (interactive) (text-scale-decrease 1)))
(global-set-key (kbd "C-0") (lambda () (interactive) (text-scale-set 0)))

(global-set-key [?\C-x ?/] 'point-to-register) ; quicker than the default binding
(global-set-key [?\C-x ?j] 'jump-to-register)  ; quicker than the default binding

;; Make mouse wheel to scroll a constant amount each click. This is less confusing than the stupid variable amount of scrolling
;; (which is faster, but becomes impossible to track quickly with my eyes).
(global-set-key [mouse-4] (lambda () (interactive) (scroll-down 5)))
(global-set-key [mouse-5] (lambda () (interactive) (scroll-up 5)))

;; Control-Z to suspend emacs is stupid when running from a window manager. It has the same effect as exiting from emacs (since
;; there's no terminal from which you can resume it) but it doesn't offer to save any files. With i3 emacs enters some kind of weird
;; state where teh window flashes if you press C-g but doesn't respond to anything else.  This is made even worse by the fact that
;; C-z is right next to C-x on Qwerty and Colemak keyboards.
(global-unset-key (kbd "C-z"))

;; Extra bindings for programmable keyboards
(global-set-key (kbd "C-M-s-b") 'ido-switch-buffer)
(global-set-key (kbd "C-M-s-c") 'compile)
(global-set-key (kbd "C-M-s-e") 'next-error)
(global-set-key (kbd "C-M-s-g") 'grep)
(global-set-key (kbd "C-M-s-l") 'goto-line)
(global-set-key (kbd "C-M-s-s") 'delete-other-windows)
(global-set-key (kbd "C-M-s-o") 'other-window)
(global-set-key (kbd "C-M-s-z") 'undo)
(global-set-key (kbd "C-M-s-t") 'mc/mark-next-like-this)
(global-set-key (kbd "C-M-s-y") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-M-s-h") 'mc/edit-lines)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Smart mode line
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(unless (package-installed-p 'smart-mode-line)
  (package-install 'smart-mode-line))

(use-package smart-mode-line
             :config
             (sml/setup))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; multiple-cursors -- edit in multiple places at once
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(unless (package-installed-p 'multiple-cursors)
  (package-install 'multiple-cursors))

(use-package multiple-cursors)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Helm -- completion mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(unless (package-installed-p 'helm)
  (package-install 'helm))

(require 'helm)
(require 'helm-autoloads)

;; The default "C-x c" is quite close to "C-x C-c" which quits Emacs. Therefore, change the helm prefix to "C-c h". Note that we
;; must set the key globally because we cannot change helm-command-prefix-key once helm-config is loaded.
(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-unset-key (kbd "C-x c"))

(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "C-x b") 'helm-mini)
(global-set-key (kbd "C-c h o") 'helm-occur)
(global-set-key (kbd "C-h a") 'helm-apropos)
(global-set-key (kbd "C-h SPC") 'helm-all-mark-rings)

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action)   ; make TAB work in terminal
(define-key helm-map (kbd "C-z") 'helm-select-action)               ; list actions using C-z

(when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

(setq
 ;helm-full-frame 1
 helm-buffers-fuzzy-matching t
 helm-recent-fuzzy-match t
 helm-M-x-fuzzy-match t
 helm-split-window-inside-p t         ; open helm buffer inside current window, not occupy whole other window
 ;helm-move-to-line-cycle-in-source t     ; move to end or beginning of source when reaching top or bottom of source
 ;helm-ff-search-library-in-sexp t            ; search for library in 'require and 'declare-function sexp
 ;helm-scroll-amount 8                ; scroll this many lines other window using M-<next> and M-<prior>
 helm-ff-file-name-history-use-recentf t
 helm-echo-input-in-header-line t)

(defun robb/helm-hide-minibuffer-maybe ()
  "Hide minibuffer in Helm session if we use the header line as input field."
  (when (with-helm-buffer helm-echo-input-in-header-line)
    (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
      (overlay-put ov 'window (selected-window))
      (overlay-put ov 'face
                   (let ((bg-color (face-background 'default nil)))
                     `(:background ,bg-color :foreground ,bg-color)))
      (setq-local cursor-type nil))))

(add-hook 'helm-minibuffer-set-up-hook 'robb/helm-hide-minibuffer-maybe)

;; Limit the size of the helm buffer so that the first line, which is echoing what we're typing, is not jumping around
;; vertically on the screen as the number of matches changes.
(setq helm-autoresize-max-height 0
      helm-autoresize-min-height 40)

;; So that we can use helm to search for Unix man pages.
(add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)

(helm-autoresize-mode 1)
(helm-mode 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Key mode -- interactively shows bindings for prefix keys
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(unless (package-installed-p 'which-key)
  (package-install 'which-key))

(use-package which-key
  :config (which-key-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Org Mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(unless (package-installed-p 'ox-gfm)      ; Markdown exporter
  (package-install 'ox-gfm))
(use-package ox-gfm)

(unless (package-installed-p 'ox-asciidoc) ; Asciidoc exporter
  (package-install 'ox-asciidoc))
(use-package ox-asciidoc)

(unless (package-installed-p 'ox-minutes)  ; plain text exporter for meeting minutes
  (package-install 'ox-minutes))
(use-package ox-minutes)

(setq org-hide-leading-stars t)
(setq org-odd-levels-only t)
(setq org-log-done 'time)
(setq org-agenda-files '("~/Notes"))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Magit (git in emacs)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(unless (package-installed-p 'magit)
  (package-install 'magit))

(use-package magit
    :config
  (defadvice magit-status (after robb-magit-fullscreen activate)
    "Open the magit-status window in full frame mode instead of splitting the current window."
    (delete-other-windows)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ws-butler -- unobtrusive whitespace cleanup at ends of lines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(unless (package-installed-p 'ws-butler)
  (package-install 'ws-butler))

(use-package ws-butler)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Rust
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(unless (package-installed-p 'rustic)
  (package-install 'rustic))

(use-package rustic
    :ensure t
    :config
    (setq rustic-format-on-save nil)
    :custom
    (rustic-cargo-use-last-stored-arguments t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; C/C++
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(unless (package-installed-p 'cmake-mode)
  (package-install 'cmake-mode))
(use-package cmake-mode)

;; Treat ".h" files as C++ instead of C
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

;; Robb's C++ style
(defconst rpm-c-style
  `(
    (c-style-variables-are-local-p     . t)
    (c-tab-always-indent               . nil)
    (c-basic-offset                    . 4)
    (c-echo-syntactic-information-p    . t)
    (c-indent-comments-syntactically-p . t)
    (c-hanging-comment-starter-p       . nil)
    (c-hanging-comment-ender-p         . nil)
    (c-backslash-column                . ,(- (frame-width) 5))
    (c-doc-comment-style               . javadoc)

    ;; Linefeeds before and/or after braces?
    (c-hanging-braces-alist     . (
                                   (block-open before after)
                                   (block-close . c-snug-do-while)
                                   (brace-list-open)
                                   (brace-list-close)
                                   (brace-list-intro)
                                   (brace-list-entry)
                                   (class-open after)
                                   (class-close before)
                                   (defun-open after)
                                   (defun-close before after)
                                   (inline-open after)
                                   (inline-close before after)
                                   (substatement-open after)
                                   (substatement-case-open after)
                                   (extern-lang-open after)
                                   (extern-lang-close before after)
                                   (namespace-open after)
                                   (namespace-close before after)
                                   (statement-case-open after)
                                   (statement-case-close before after)
                                   ))

    ;; Linefeeds before and/or after colons?
    (c-hanging-colons-alist     . (
                                   (access-label after)
                                   (case-label after)
                                   (inher-intro)
                                   (label after)
                                   (member-init-intro before)
                                   ))

    ;; What happens for `#' signs?
    (c-electric-pound-behavior  . (alignleft))
                           
    ;; Cleanup actions...
    (c-cleanup-list             . (brace-else-brace
                                   brace-elseif-brace
                                   brace-catch-brace
                                   empty-defun-braces
                                   defun-close-semi
                                   list-close-comma
                                   scope-operator
                                   ))

    ;; Offsets
    (c-offsets-alist            . (
                                   (access-label          . -)
                                   (arglist-intro         . c-lineup-arglist-intro-after-paren)
                                   (arglist-cont          . 0)
                                   (arglist-cont-nonempty . c-lineup-arglist)
                                   (arglist-close         . c-lineup-arglist)
                                   (block-open            . 0)
                                   (block-close           . 0)
                                   (brace-list-open       . 0)
                                   (brace-list-close      . 0)
                                   (brace-list-intro      . +)
                                   (brace-list-entry      . 0)
;                                  (c                     . rpmc-c-no-indent)
                                   (case-label            . +)
                                   (class-open            . 0)
                                   (comment-intro         . c-lineup-comment)
                                   (cpp-macro             . -1000)
                                   (defun-open            . 0)
                                   (defun-close           . 0)
                                   (defun-block-intro     . +)
                                   (do-while-closure      . 0)
                                   (else-clause           . 0)
                                   (extern-lang-open      . 0)
                                   (extern-lang-close     . 0)
                                   (friend                . 0)
                                   (func-decl-cont        . +)
                                   (inclass               . +)
                                   (innamespace           . 0)
                                   (inextern-lang         . 0)
                                   (inher-intro           . +)
                                   (inher-cont            . c-lineup-multi-inher)
                                   (inline-open           . +)
                                   (inline-close          . 0)
                                   (knr-argdecl-intro     . 5)
                                   (knr-argdecl           . 0)
                                   (label                 . 0)
                                   (member-init-intro     . +)
                                   (member-init-cont      . 0)
                                   (namespace-open        . -)
                                   (objc-method-intro     . -1000)
                                   (objc-method-args-cont . c-lineup-ObjC-method-args)
                                   (objc-method-call-cont . c-lineup-ObjC-method-call)
                                   (statement             . 0)
                                   (statement-case-intro  . +)
                                   (statement-cont        . c-lineup-math)
                                   (stream-op             . c-lineup-streamop)
                                   (string                . -1000)
                                   (substatement-open     . +)
                                   (topmost-intro         . 0)
                                   (topmost-intro-cont    . 0)
                                   ))
    )
  "Robb Matzke C/C++ Programming Style")

;; Selectively hides C/C++ 'if' and 'ifdef' regions.
(setq hide-ifdef-mode-hook
      (lambda ()
        (if (not hide-ifdef-define-alist)
            (setq hide-ifdef-define-alist
                  '((rose
                     (ROSE_ENABLE_ASM_AARCH32 . 1)
                     (ROSE_ENABLE_ASM_AARCH64 . 1)
                     (ROSE_ENABLE_BINARY_ANALYSIS . 1)
                     (ROSE_ENABLE_CONCOLIC_TESTING . 1)
                     (ROSE_ENABLE_DEBUGGER_GDB . 1)
                     (ROSE_ENABLE_DEBUGGER_LINUX . 1)
                     (ROSE_ENABLE_LIBRARY_IDENTIFICATION . 1)
                     (ROSE_ENABLE_MODEL_CHECKER . 1)
                     (ROSE_ENABLE_SARIF . 1)
                     (ROSE_ENABLE_SIMULATOR . 1)
                     (ROSE_HAVE_BOOST_SERIALIZATION_LIB . 1)
                     (ROSE_HAVE_CEREAL . 1)
                     (ROSE_HAVE_DLIB . 1)
                     (ROSE_HAVE_LIBDWARF . 1)
                     (ROSE_HAVE_Z3 . 1)
                     (ROSE_SUPPORTS_SERIAL_IO . 1)
                     (SIEVE_ENABLE_UI . 1)
                     (__linux . 1)))))
        (hide-ifdef-use-define-alist 'rose) ; use this list by default
        ))

(defun rpm-c-mode-hook ()
  ;; Add my personal style and set it for the current buffer
  (c-add-style "rpm" rpm-c-style)
  (c-set-style "rpm")

  ;; Other customizations
  (setq tab-width 8)                    ; The normal tab width
  (setq indent-tabs-mode nil)           ; insert SPC rather than TAB characters
  (setq truncate-lines t)
  (ws-butler-mode 1)

  ;; We could be fancier here. The extra "bash" wrapping is because commands don't always work when invoked directly on remote
  ;; buffers.
  (make-local-variable 'compile-command)
  (setq compile-command "bash -c 'rmc -C ~/rose-wip/rose/_build make'")
  (setq compilation-context-lines 0)

  ;; The ROSE standard is 132 columns maximum
  (make-local-variable 'fill-column)
  (setq fill-column 132)
  (display-fill-column-indicator-mode)
  (setq c-backslash-column 132)
  (setq c-backslash-max-column c-backslash-column)

  (c-toggle-auto-hungry-state 1)        ; Del and C-d eat white space aggressively
  (pilf-mode 1)                         ; Robb's program intra-line formatting mode (horizontal white space in a line)
  (setq comment-column (or (pilf-get-config 'trailing-comment-column) 64))

  (hide-ifdef-mode 1)

  ;; Keybindings for all supported languages.  We can put these in c-mode-map because c++-mode-map and java-mode-map inherit from
  ;; it.
  (define-key c-mode-map "\C-m" 'newline-and-indent)
  (define-key c-mode-map "\C-j" 'newline)

  ;; Filling (automatic line wrapping) of comments and mult-line strings.
  (c-setup-filladapt)
  (filladapt-mode 1)
  (auto-fill-mode 1))

(load "pilf")                           ;Robb's new C/C++ minor mode (2010)
(add-hook 'c-mode-hook 'rpm-c-mode-hook)
(add-hook 'c++-mode-hook 'rpm-c-mode-hook)

;; Tup build system files
(load-file "~/.emacs.d/lisp/tup-mode.el")

; Allow ANSI color code escapes in the compile window
(require 'ansi-color)
(defun colorize-compilation-buffer ()
  (toggle-read-only)
  (ansi-color-apply-on-region compilation-filter-start (point))
  (toggle-read-only))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

;;OLD;; 
;;OLD;; ;; Perl programming
;;OLD;; (setq perl-indent-level 4)
;;OLD;; 
;;OLD;; (defun rpm-d-mode-hook ()
;;OLD;;   (setq tab-width 8)
;;OLD;;   (setq indent-tabs-mode nil)
;;OLD;;   (setq truncate-lines t)
;;OLD;;   (setq fill-column (- 132 5))
;;OLD;;   (setq comment-column 64)
;;OLD;;   (ws-butler-mode 1))
;;OLD;; (add-hook 'd-mode-hook 'rpm-d-mode-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Miscellaneous editing functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun duplicate-line (arg)
  "Duplicate current line, leaving point in lower line."
  (interactive "*p")

  ;; save the point for undo
  (setq buffer-undo-list (cons (point) buffer-undo-list))

  ;; local variables for start and end of line
  (let ((bol (save-excursion (beginning-of-line) (point)))
        eol)
    (save-excursion

      ;; don't use forward-line for this, because you would have
      ;; to check whether you are at the end of the buffer
      (end-of-line)
      (setq eol (point))

      ;; store the line and disable the recording of undo information
      (let ((line (buffer-substring bol eol))
            (buffer-undo-list t)
            (count arg))
        ;; insert the line arg times
        (while (> count 0)
          (newline)         ;; because there is no newline in 'line'
          (insert line)
          (setq count (1- count)))
        )

      ;; create the undo information
      (setq buffer-undo-list (cons (cons eol (point)) buffer-undo-list)))
    ) ; end-of-let

  ;; put the point in the lowest line and return
  (next-line arg))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Custom variables
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(custom-enabled-themes '(smart-mode-line-light))
 '(custom-safe-themes
   '("3fe4861111710e42230627f38ebb8f966391eadefb8b809f4bfb8340a4e85529" "45631691477ddee3df12013e718689dafa607771e7fd37ebc6c6eb9529a8ede5" default))
 '(display-time-mode t)
 '(package-selected-packages
   '(dockerfile-mode yaml-mode ox-mediawiki cmake-mode lsp-mode seq seq-2\.24 gnu-elpa-keyring-update magit which-key helm smart-mode-line tabbar session pod-mode muttrc-mode mutt-alias markdown-mode initsplit htmlize graphviz-dot-mode folding eproject diminish csv-mode company color-theme-modern browse-kill-ring boxquote bm bar-cursor apache-mode))
 '(tool-bar-mode nil)
 '(transient-mark-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Hack Nerd Font Mono" :foundry "SRC" :slant normal :weight normal :height 77 :width normal)))))
